# AF Msg Constructor Changelog

v1.0.1.3 (2021/11/01)
-Official public release

v1.0.1.2 (2021/08/10) (private release)
-Fixed a bug when creating a constructor on a message with 0 inputs

v1.0.0.1 (2021/07/16) (private release)
-Initial Zyah release